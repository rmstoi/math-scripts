#!/usr/bin/env python3

'''Mathematics exercise'''

# Copyright (c) 2020 Ruslan Mstoi <ruslan.mstoi@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import random
import operator
import argparse
import datetime
import sys


class AnsiSeqSGR(object):
    """Select Graphic Rendition

    Interface to ANSI escape codes to Select Graphic Rendition parameters

    """

    def __init__(self):
        self._reset = "\x1B[0m"
        self._fg_blue = "\x1B[94m"
        self._fg_green = "\x1B[32m"
        self._fg_red = "\x1B[31m"

    def set_fg_color(self, color_seq, text):
        """Set SGR text foreground color

        color_seq -- escape sequence to set color"""
        return "%s%s%s" % (color_seq, text, self._reset)

    def red(self, text):
        """Return red text"""
        return self.set_fg_color(self._fg_red, text)

    def green(self, text):
        """Return green text"""
        return self.set_fg_color(self._fg_green, text)

    def blue(self, text):
        """Return blue text"""
        return self.set_fg_color(self._fg_blue, text)


SGR = AnsiSeqSGR()


class Statistics(object):
    """Result statistics"""

    def __init__(self):
        """Constructor of statistics"""
        self.correct = 0
        self.wrong = 0
        self.total = 0
        self.start_time = datetime.datetime.now()

    def update(self, answer, correct_answer):
        """Update statistics

        answer -- An answer given by the user
        correct_answer -- Correct answer to the problem

        """
        self.total += 1

        if answer == correct_answer:
            self.correct += 1
            print(SGR.green("Correct!"))
        else:
            self.wrong += 1
            print("%s Correct answer is %d" %
                  (SGR.red("Wrong!"), correct_answer,))

        print("total=%d correct=%d wrong=%d time=%s" %
              (self.total, self.correct, self.wrong,
               datetime.datetime.now() - self.start_time))

    def __str__(self):
        '''String representation method.'''
        out = "\n\n"
        out += "Total: %d\n" % self.total
        out += SGR.green("Correct answers: %d\n" % self.correct)
        out += SGR.red("Wrong answers: %d\n" % self.wrong)
        out += "Time: %s\n" % (datetime.datetime.now() - self.start_time,)
        return out


STATS = Statistics()

# use 0 to ask problems with multiplication by 0 and 1
MIN_NUMBER = 2

MAX_NUMBER = 9

# all supported ops
ALL_OPS = {
    "+": operator.add,
    "-": operator.sub,
    "*": operator.mul,
    "/": operator.truediv
}

# default used ops
OPS_STR = "+-"

# calculated in main, from OPS_STR
OPS = None


def int_input(prompt):
    """Read integer from stdin"""
    answer = input(prompt)

    try:
        answer = int(answer)
    except ValueError as e:
        print("Please enter integer!!! (Exception: %s)" % e)
        return int_input(prompt)

    return answer


def get_x_y_op():
    """Return random x, y and operator"""
    op = random.choice("".join(OPS.keys()))

    while True:
        numbers = [random.randint(MIN_NUMBER, MAX_NUMBER) for _ in range(2)]

        # prevent negative subtraction result and division of smaller number by
        # bigger
        if op in ("/", "-"):
            numbers.sort(reverse=True)

        x, y = numbers

        if op == "/":
            if y == 0:
                continue
            if x % y != 0:  # result not integer (whole number)
                continue
        break

    return x, y, op


def ask_problem(x=None, y=None, op=None):

    if not x:  # x, y, op are None
        x, y, op = get_x_y_op()

    correct_answer = OPS[op](x, y)
    answer = int_input("%d %s %d = " % (x, op, y))

    STATS.update(answer, correct_answer)

    if answer != correct_answer:
        ask_problem(x, y, op)  # rehearse


ask_problem.count = 0


def parse_arguments():
    """Parse command line arguments"""
    global MIN_NUMBER, MAX_NUMBER, OPS_STR, OPS

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--min", type=int, dest="min_number",
                        default=MIN_NUMBER,
                        help="Smallest number, default=%(default)d")
    parser.add_argument("-x", "--max", type=int, dest="max_number",
                        default=MAX_NUMBER,
                        help="Biggest number, default=%(default)d")
    parser.add_argument("-o", "--ops",
                        default=OPS_STR,
                        help="Operators. supported are +-*/ "
                        "default=%(default)r")
    args = parser.parse_args()

    if args.min_number != MIN_NUMBER:
        MIN_NUMBER = args.min_number
    if args.max_number != MAX_NUMBER:
        MAX_NUMBER = args.max_number

    if MIN_NUMBER >= MAX_NUMBER:
        sys.exit("min_number=%d max_number=%d, max_number should be "
                 "bigger than min_number!" % (MIN_NUMBER, MAX_NUMBER))

    if args.ops:
        if len(args.ops) != len(set(args.ops)):
            sys.exit("ops=%s, some ops are repeated", args.ops)
        all_ops_str = "".join(ALL_OPS.keys())
        new_ops_str = ""
        for op in args.ops:
            if op in all_ops_str:
                new_ops_str += op
            else:
                print("Ignoring invalid op", op)

        if new_ops_str:
            OPS_STR = new_ops_str

    OPS = {op: ALL_OPS[op] for op in OPS_STR if op in ALL_OPS}


def main():
    """Main"""
    parse_arguments()

    while True:
        ask_problem()


if __name__ == "__main__":
    try:
        main()
    except (KeyboardInterrupt, EOFError) as e:
        print(STATS)
        print("Bye!")
