#!/usr/bin/env python3

'''Multiplication table printer'''

# Copyright (c) 2020 Ruslan Mstoi <ruslan.mstoi@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import sys


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-x", "--max", type=int, dest="max_number", default=9,
                        help="Biggest number, default=%(default)d")
    args = parser.parse_args()

    if args.max_number <= 0:
        sys.exit("Max number too small!")

    return args


def main():
    args = parse_arguments()
    max_number = args.max_number + 1

    for y in range(1, max_number):
        print("%2d" % y, end=' ')
        for x in range(2, max_number):
            # TODO align based on number of digits
            print("%2d" % (x * y), end=' ')
        print()


if __name__ == "__main__":
    main()
